package com.revature.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="POST")
public class Post {
	
	@Id
	@GeneratedValue
	@Column(name="P_ID")
	private int id;
	
	@Column(name="P_CONTENT")
	private String content;
	
	@Column(name="P_LIKEDBY")
	@ManyToMany(fetch = FetchType.LAZY)
	private List<User> userWhoLiked;
	
	@Column(name="P_POSTCREATOR")
	@OneToOne(fetch = FetchType.EAGER)
	private int postCreator;
	
	@Column(name="P_IMAGE")
	private String postImage;
	
	@Column(name="P_LIKECOUNT")
	private int likeCount;
	
	public Post(){}

	public Post(int id, String content, List<User> userWhoLiked, int postCreator, String postImage, int likeCount) {
		super();
		this.id = id;
		this.content = content;
		this.userWhoLiked = userWhoLiked;
		this.postCreator = postCreator;
		this.postImage = postImage;
		this.likeCount = likeCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<User> getUserWhoLiked() {
		return userWhoLiked;
	}

	public void setUserWhoLiked(List<User> userWhoLiked) {
		this.userWhoLiked = userWhoLiked;
	}

	public int getPostCreator() {
		return postCreator;
	}

	public void setPostCreator(int postCreator) {
		this.postCreator = postCreator;
	}

	public String getPostImage() {
		return postImage;
	}

	public void setPostImage(String postImage) {
		this.postImage = postImage;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", content=" + content + ", userWhoLiked=" + userWhoLiked + ", postCreator="
				+ postCreator + ", postImage=" + postImage + ", likeCount=" + likeCount + "]";
	};
}
