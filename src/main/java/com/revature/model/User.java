package com.revature.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="USER",
uniqueConstraints ={@UniqueConstraint (columnNames = {"U_USERNAME"})})

public class User {
	
	@Id
	@GeneratedValue
	@Column(name="U_ID")
	private int id;
	
	@Column(name="U_USERNAME")
	private String username;
	
	@Column(name="U_PASSWORD")
	private String password;
	
	@Column(name="U_EMAIL")
	private String email;
	
	@Column(name="U_FIRSTNAME")
	private String firstName;
	
	@Column(name="U_LASTNAME")
	private String lastName;
	//check if correct
	
	@Column(name="U_PROFILEPICTURE")
	private String profilePicture; 
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "userWhoLiked")
	private List<Post> likedPostList;
	
	public User(){};
	
	//stores all u
	public User(String username, String password){
		this.username = username;
		this.password = password;
	}

	public User(int id, String username, String password, String email, String firstName, String lastName,
			String profilePicture, List<Post> likedPostList) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName.toUpperCase();
		this.lastName = lastName.toUpperCase();
		this.profilePicture = profilePicture;
		this.likedPostList = likedPostList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<Post> getLikesPostId() {
		return likedPostList;
	}

	public void setLikesPostId(List<Post> likedPostList) {
		this.likedPostList = likedPostList;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", profilePicture=" + profilePicture + ", likesPostId=" + likedPostList + "]";
	}
	
}
